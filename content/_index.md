+++
title = "index"
author = ["Sherlockes"]
date = 2023-08-30
lastmod = 2023-08-31T03:52:57+02:00
draft = false
weight = 5
thumbnail = "images/image.jpg"
toc = true
+++

Esta es mi base de conocimiento generada gracias a Org-Roam. Todo en una carpeta con archivos de texto en org-mode. Aquí tienes el [índice]({{< relref"notas/_index.md" >}}) y a continuación las últimas notas modificadas.
