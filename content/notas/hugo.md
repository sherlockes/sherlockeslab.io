+++
title = "hugo"
author = ["Sherlockes"]
date = 2023-09-04T13:40:00+02:00
lastmod = 2024-04-18T14:47:31+02:00
tags = ["hugo"]
categories = ["apps"]
draft = false
weight = 5
thumbnail = "images/hugo.png"
toc = true
+++

Hugo es un moderno framework para creación de sitios web de propósito general. Se ubica en la categoría de los nuevos generadores de sitios estáticos,​ basados en la arquitectura dinámica JAMstack y es escrito completamente en Go.

<!--more-->


## MarkDown {#markdown}

-   [Lista](https://www.markdownguide.org/tools/hugo/) de comandos permitidos.
-   [Cheatsheet](https://makewithhugo.com/markdown-basics/)


## Crear un enlace interno {#crear-un-enlace-interno}

-   En el mismo directorio: `[Titulo]({{</* relref"archivo.md" */>}})`
-   En un directorio absoluto: `[Titulo]({{</* ref"/dir/archivo.md" */>}})`
-   A un apartado del post: `[Foo]({{</* ref "#foo" */>}})`
-   A un apartado en un directorio absoluto: `[Titulo]({{</* ref"/dir/archivo.md#foo-" */>}})`

    > Muy importante para este último caso el guión al final del apartado, de no estar, no funciona


## Clonar el repositorio {#clonar-el-repositorio}

git clone --recurse-submodules git@gitlab.com:sherlockes/sherlockes.gitlab.io.git


## Insertar {#insertar}

-   Comentario en plantilla: `{{/* This is my comment */}}`
-   Tag "more": `<!--more-->`
-   Trozo de código en línea: `~Aquí el código~`


## Server {#server}

-   Server en red local: `hugo server --bind=192.168.10.202 --baseURL=http://192.168.10.202:1313`

    > Con el parámetro "baseURL" conseguimos que el servidor no muestre el nombre de la carpeta.
-   Ver contenido modificado: `hugo server -D --navigateToChanged`


## Shortcodes {#shortcodes}

-   Escapar shortcode para no evaluarlo: `{{</*/* youtube w7iekruei7 */*/>}}`
-   Video de Youtube: `{{</* youtube w7iekruei7 */>}}`


### Crear un trozo en borrador {#crear-un-trozo-en-borrador}

```jinja2
{{</* borrador */>}}
Aquí iremos añadiendo el contenido con el que queremos actualizar la entrada...
{{</* / borrador */>}}
```


## TOC - Seleccionar encabezados a mostrar {#toc-seleccionar-encabezados-a-mostrar}

```toml
[markup]
  [markup.tableOfContents]
    endLevel = 3
    ordered = false
    startLevel = 2
```


## Ordenar post por fecha de modificación {#ordenar-post-por-fecha-de-modificación}

En el archivo config.toml añadir lo siguiente para que se genere la variable "lastmod"

```toml
enableGitInfo = true

[frontmatter]
  lastmod = ["lastmod", ":git", "date", "publishDate"]
```

Con esto editaremos el archivo "index.html" del tema para que la línea que genera el Paginator pase de esto

```html
{{ $paginator := .Paginate (where .Site.RegularPages "Type" "in" .Site.Params.mainSections) }}
```

a esto

```html
{{ $paginator := .Paginate (where .Site.RegularPages "Type" "in" .Site.Params.mainSections).ByLastmod.Reverse }}
```
