+++
title = "org-roam plantillas"
author = ["Sherlockes"]
date = 2023-08-30T00:00:00+02:00
lastmod = 2023-09-11T11:06:19+02:00
tags = ["org-roam"]
categories = ["emacs"]
draft = false
weight = 5
thumbnail = "images/org-roam.png"
toc = true
+++

Cada vez que se añade una nueva nota en [org-roam]({{< relref "org_roam.md" >}}) se hace uso de las plantillas que estan definidas en la configuración de [emacs]({{< relref "emacs.md" >}}). Aquí está la información que he ido recopilando y las plantillas que utilizo.

<!--more-->


## Plantillas {#plantillas}

Se definen con la variable (org-roam-capture-templates)

> Existe un bug que no permite añadir una nueva línea editanto la variable anterior, hay que hacerlo en el archivo de configuración mediante "\n".


## Mis plantillas en "init.el" {#mis-plantillas-en-init-dot-el}

Dentro del apartado "custom-set-variables

```elisp
'(org-roam-capture-templates
   (quote
    (("d" "default" plain "%?" :target
      (file+head "%<%Y%m%d>-${slug}.org" "#+title: ${title}
#+STARTUP: show2levels
")
```
