+++
title = "Raspberry"
author = ["Sherlockes"]
date = 2023-12-15
lastmod = 2023-12-16T20:12:22+01:00
tags = ["raspberry"]
categories = ["sos"]
draft = false
weight = 5
thumbnail = "images/raspberry.png"
toc = true
+++

Todo sobre Raspberry

<!--more-->


## [Proceso de instalación]({{< relref "raspberry_instalacion.md" >}}) {#proceso-de-instalación--raspberry-instalacion-dot-md}


## Argon One Pi4 V2 (Caja Raspberry Pi) {#argon-one-pi4-v2--caja-raspberry-pi}


### Jumper pin settings {#jumper-pin-settings}

-   Pin 1-2: Modo por defecto, hay que pulsar el botón "ON" para encender
-   Pin 2-3: Modo siempre on, no hay que pulsar para encender
-   Argon ONE Pi 4 script 'curl <https://download.argon40.com/argon1.sh%7Cbash>'
-   Configurar utilidad 'argonone-config'
-   Desinstalar utilidad 'argonone-uninstall'


## Raspberry Pi 3B+ {#raspberry-pi-3b-plus}


### Configurar usb como método de arranque {#configurar-usb-como-método-de-arranque}

-   Instalar raspbian desde una microsd
-   Ejecutar los siguientes comandos

<!--listend-->

```nil
sudo apt update && sudo apt upgrade && sudo reboot
echo program_usb_boot_mode=1 | sudo tee -a /boot/config.txt
sudo reboot
```
