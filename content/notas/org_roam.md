+++
title = "org-roam"
author = ["Sherlockes"]
date = 2023-08-30T00:00:00+02:00
lastmod = 2023-09-14T12:57:59+02:00
tags = ["org-roam"]
categories = ["emacs"]
draft = false
weight = 5
thumbnail = "images/org-roam.png"
toc = true
+++

Org-Roam en una extensión de [Emacs]({{< relref "emacs.md" >}}) diseñada para facilitar la toma de notas y la organización de información en forma de documentos interconectados. Está especialmente diseñada para usuarios que emplean [Org-Mode]({{< relref "org_mode.md" >}}) en [Emacs]({{< relref "emacs.md" >}}), que es un sistema de organización y gestión de tareas altamente personalizable.

<!--more-->


## [Primeros pasos]({{< relref "org_roam_primeros_pasos.md" >}}) {#primeros-pasos--org-roam-primeros-pasos-dot-md}


## [Comandos esenciales]({{< relref "org_roam_comandos_esenciales.md" >}}) {#comandos-esenciales--org-roam-comandos-esenciales-dot-md}


## [Plantillas]({{< relref "org_roam_plantillas.md" >}}) {#plantillas--org-roam-plantillas-dot-md}


## Sincronizar con repositorio en Github {#sincronizar-con-repositorio-en-github}

Si la carpeta "org-roam" ya está creada se cambia el directorio de trabajo a la misma y se actualiza el repositorio. Si la carpeta no está creada descarga el repositorio

```elisp
  (if (file-exists-p "~/org-roam/")
    (let ((default-directory "~/org-roam"))(shell-command "git pull"))
  (let ((default-directory "~/"))(shell-command "git clone git@github.com:sherlockes/org-roam.git"))
)
```

Esto, al estar introducido en el archivo "init.el" se ejecutará al arrancar Emacs.

Para conseguir que se sincronice tras modificar algún archivo usaremos un "hook" y una función auxiliar que simplemente comprueba si el archivo que se ha modificado está dentro de la carpeta "org-roam". En caso de que se de la condición, se ejecutará otra función que actualiza el repositorio.

```elisp
(defun org-roam-update()
  (interactive)

      (let ((default-directory "~/org-roam"))
          (shell-command "git add --all")
          (shell-command "git commit -m 'Update'")
          (shell-command "git push")
      )
  )
(defun funcion-al-guardar ()
  (let ((directorio-org-roam (expand-file-name "org-roam" (getenv "HOME"))))
    (when (string-prefix-p directorio-org-roam buffer-file-name)
      (org-roam-update))))

(add-hook 'after-save-hook 'funcion-al-guardar)
```


## Exportar org-roam-ui a una web estática {#exportar-org-roam-ui-a-una-web-estática}

Todavía no he sido capaz de conseguirlo, pero dejo aquí todos los enlaces y avances.

-   Notas con la exportación de [wylited](https://github.com/wylited/notes) a partir del repo de [jgru](https://github.com/jgru/org-roam-ui/tree/add-export-capability). No da ningún error pero en el html generado sólo se ve el color de fondo.

-   Este [repo](https://github.com/uncomfyhalomacro/org-roam-ui/tree/feature/add-export-functionality) de uncomfyhalomacro todavía no lo he probado
