+++
title = "org-roam comandos esenciales"
author = ["Sherlockes"]
date = 2023-08-30T00:00:00+02:00
lastmod = 2023-09-11T11:47:41+02:00
tags = ["org-roam"]
categories = ["emacs"]
draft = false
weight = 5
thumbnail = "images/org-roam.png"
toc = true
+++

Además de los utilizados en [emacs]({{< relref "emacs.md" >}}), este es un resumen con los comandos esenciales de [org-roam]({{< relref "org_roam.md" >}}) que uso habitualmente.

<!--more-->

-   Buffer Org-Roam, Mostrar (org-roam-nbuffer-toggle) -&gt; C-c n l
-   Enlace, Insertar (org-roam-node-insert) -&gt; C-c n i
-   Enlace, Manual -&gt; [[]] M-x completion-at-point
-   Nodo, Buscar o crear (org-roam-node-find) -&gt; C-c n f
-   Nodo, Crear a partir de encabezado (org-id-get-create)
-   Nodo, Crear un alias (org-roam-alias-add)
-   Nodo, Anadir un tag (org-roam-tag-add)
