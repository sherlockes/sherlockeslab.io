+++
title = "nemo"
author = ["Sherlockes"]
date = 2023-08-29
lastmod = 2023-09-10T19:15:06+02:00
tags = ["nemo", [",", "linux"]]
categories = ["apps"]
draft = false
weight = 5
thumbnail = "images/image.jpg"
toc = true
+++

Nemo es un software gratuito y de código abierto y administrador oficial de archivos del entorno de escritorio Cinnamon. Es una bifurcación de archivos GNOME (anteriormente llamados Nautilus).

<!--more-->

Explorador de archivos que utilizo en [linux mint]({{< relref "linux_mint.md" >}}).


## Comandos básicos {#comandos-básicos}

-   Crear enlace simbólico: Ctrl + Shift + Arrastrar (Editar-Preferencias-Menús contextuales)
