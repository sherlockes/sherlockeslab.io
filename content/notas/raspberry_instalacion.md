+++
title = "Raspberry - Instalación"
author = ["Sherlockes"]
date = 2023-12-15
lastmod = 2024-02-08T21:00:19+01:00
tags = ["raspberry"]
categories = ["sos"]
draft = false
weight = 5
thumbnail = "images/raspberry.png"
toc = true
+++

Partimos con la [raspberry]({{< relref "raspberry.md" >}}) vacía y estos son los pasos que hay que dar. He probado a hacer un script que con el paso del tiempo siempre falla en un punto u otro, así quedan las instrucciones claras.

<!--more-->


## Instalando PI OS Lite {#instalando-pi-os-lite}


### Grabar la imagen mediante [Imager](https://www.raspberrypi.com/software/) y actualizar el sistema. {#grabar-la-imagen-mediante-imager-y-actualizar-el-sistema-dot}

```bash
sudo apt update
sudo apt upgrade
```


### Generar la llave ssh {#generar-la-llave-ssh}

```bash
ssh-keygen
```


### Copiar la llave ssh a [Github](https://github.com/settings/keys) y [GitLab](https://gitlab.com/-/profile/keys). {#copiar-la-llave-ssh-a-github-y-gitlab-dot}

Abrir el archivo ".ssh/id_rsa.pub" con un editor de texto y copiar el contenido.


### Instalar y configurar git {#instalar-y-configurar-git}

```bash
sudo apt install git
git config --global user.email "sherlockes@yahoo.es"
git config --global user.name "Sherlockes"
```


### Clonar SherloScripts {#clonar-sherloscripts}

```bash
git clone git@github.com:sherlockes/SherloScripts.git
```


### Open Network Manager UI with para asignar ip estática {#open-network-manager-ui-with-para-asignar-ip-estática}

```bash
sudo nmtui
```


### Instalar Pi-Hole {#instalar-pi-hole}

```bash
curl -sSL https://install.pi-hole.net | bash
```


### Instalar Rclone ([Web](https://rclone.org)). {#instalar-rclone--web--dot}

```bash
sudo -v ; curl https://rclone.org/install.sh | sudo bash
```
