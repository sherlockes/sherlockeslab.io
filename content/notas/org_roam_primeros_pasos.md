+++
title = "org-roam primeros pasos"
author = ["Sherlockes"]
date = 2023-08-30T00:00:00+02:00
lastmod = 2023-09-14T12:58:07+02:00
tags = ["org-roam"]
categories = ["emacs"]
draft = false
weight = 5
thumbnail = "images/org-roam.png"
toc = true
+++

La curva de aprendizaje de [org-roam]({{< relref "org_roam.md" >}}) es complicada sin una guía adecuada que serguir, estos son los primeros pasos por donde hay que empezar si quieres crear tu base de conocimiento.

<!--more-->


## Como empezar {#como-empezar}

-   En caso de configuración compartida, no sincronizar la carpeta "elpa"
-   Instalar y activar "ivy-mode"
-   Instalar "org-roam"
-   Instalar "org-roam-ui"

> La instalación de "emacsql" como dependencia de "Org-roam" me ha dado muchos problemas de compilación. Finalmente he conseguido que funcione eliminando del sistema el compilador "gcc" e instalando "clang" tal y como se comenta en el [manual](https://www.orgroam.com/manual.html#C-Compiler) online.


## Configuración en init.el {#configuración-en-init-dot-el}

```elisp
(make-directory "~/conocimiento" t)
(setq org-roam-directory (file-truename "~/conocimiento"))
(org-roam-db-autosync-mode)
(setq org-roam-completion-system 'ivy)
(setq org-roam-completion-everywhere t)
(ivy-mode 1)

(global-set-key (kbd "C-c n f") 'org-roam-node-find)
(global-set-key (kbd "C-c n l") 'org-roam-buffer-toggle) el Buffer Org-Roam
(global-set-key (kbd "C-c n i") 'org-roam-node-insert)
(global-set-key (kbd "C-c n t") 'org-roam-tag-add)
(global-set-key (kbd "C-c n a") 'org-roam-alias-add)
(global-set-key (kbd "C-c n o") 'org-id-get-create)
```
