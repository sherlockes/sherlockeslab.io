+++
title = "linux"
author = ["Sherlockes"]
date = 2023-09-13T18:31:00+02:00
lastmod = 2024-05-01T18:51:36+02:00
tags = ["linux"]
categories = ["sos"]
draft = false
weight = 5
thumbnail = "images/linux.png"
toc = true
+++

GNU/Linux  es una familia de sistemas operativos tipo Unix compuesto por software libre y de código abierto.​ GNU/Linux surge de las contribuciones de varios proyectos de software, entre los cuales destacan GNU (iniciado por Richard Stallman en 1983) y el núcleo Linux (iniciado por Linus Torvalds en 1991).

<!--more-->


## Permisos de usuario {#permisos-de-usuario}

-   Cambiar propietario de una carpeta: `sudo chown nuevo_usuario: carpeta`
-   Cambiar propietario de carpeta recursiva: `sudo chown -R nuevo_usuario: carpeta`
-   Comprobar permisos de carpeta `ls -ld carpeta`


## Enlace simbólico -&gt; ln -s target_file link_name {#enlace-simbólico-ln-s-target-file-link-name}


## Capturas de pantalla (Atajos de teclado) {#capturas-de-pantalla--atajos-de-teclado}

-   Ctrl (Portapapeles) Alt (Ventana) Mays (area)
-   Escritorio al portapapeles - Ctrl+ImprPant
-   Area al portapapeles - Ctrl+Mays+ImprPant
-   Ventana al portapapeles - Ctrl+Alt+ImprPant
-   Guardar escritorio en directorio - ImprPant
-   Guardar Area en directorio - Mays+ImprPant
-   Guardar ventana en directorio - Alt+ImprPant.


## nmap {#nmap}


### MAC a partir de IP -&gt; sudo nmap -sP -n 192.168.1.200 {#mac-a-partir-de-ip-sudo-nmap-sp-n-192-dot-168-dot-1-dot-200}


## Ubicación de un comando -&gt; type -a "comando" {#ubicación-de-un-comando-type-a-comando}


## Wireguard ([Instrucciones](https://alexpro.sytes.net/cliente-wireguard-linux/)) {#wireguard--instrucciones}

-   Instalar wireguard "sudo apt install wireguard"
-   Instalar resolvconf "sudo apt install resolvconf"
-   Copiar archivo confiuración "sudo mv mivpn.conf _etc/wireguard_"
-   Levantar conexión "sudo wg-quick up mivpn"
-   Desconectar "sudo wg-quick down mivpn"
-   Comprobar conexión "sudo wg"
