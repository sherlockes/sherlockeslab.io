+++
title = "iot"
author = ["Sherlockes"]
date = 2023-10-11T00:00:00+02:00
lastmod = 2024-01-17T17:08:23+01:00
tags = ["zigbee"]
categories = ["iot"]
draft = false
weight = 5
thumbnail = "images/iot.png"
toc = true
+++

En La domótica no es tan difícil como parece, si quieres ahorrar algo de tiempo no está de más dejar escrito lo que te ha costado un rato descubrir.

<!--more-->


## [Home Assistant]({{< relref "20240117-home_assistant.md" >}}) {#home-assistant--20240117-home-assistant-dot-md}
