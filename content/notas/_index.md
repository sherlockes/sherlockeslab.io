+++
title = "index"
author = ["Sherlockes"]
date = 2023-08-30
lastmod = 2024-02-08T21:03:32+01:00
draft = false
weight = 5
thumbnail = "images/image.jpg"
toc = true
+++

Esta es mi base de conocimiento generada gracias a [Org-Roam]({{< relref "org_roam.md" >}}). Todo en una carpeta con archivos de texto en [org-mode]({{< relref "org_mode.md" >}}).


## Emacs y alrededores {#emacs-y-alrededores}

-   [emacs]({{< relref "emacs.md" >}})
-   [org-mode]({{< relref "org_mode.md" >}})
-   [org-roam]({{< relref "org_roam.md" >}})
-   [elisp]({{< relref "elisp.md" >}})


## Lenguajes {#lenguajes}

-   [bash]({{< relref "bash.md" >}})
-   [python]({{< relref "python.md" >}})
-   [markdown]({{< relref "markdown.md" >}})
-   [regexp]({{< relref "regexp.md" >}})
-   [elisp]({{< relref "elisp.md" >}})
-   [jinja2]({{< relref "20240114-jinja.md" >}})


## Sistemas Operativos {#sistemas-operativos}

-   [linux]({{< relref "linux.md" >}})
-   [linux mint]({{< relref "linux_mint.md" >}})
-   [debian]({{< relref "debian.md" >}})


## Aplicaciones {#aplicaciones}

-   [docker]({{< relref "docker.md" >}})
-   [emacs]({{< relref "emacs.md" >}})
-   [git]({{< relref "git.md" >}})
-   [gphotos-sync]({{< relref "20240201-gphotos_sync.md" >}})
-   [hugo]({{< relref "hugo.md" >}})
-   [inkscape]({{< relref "inkscape.md" >}})
-   [nemo]({{< relref "nemo.md" >}})


## IOT {#iot}

-   [home assistant]({{< relref "20240117-home_assistant.md" >}})


## Raspberry {#raspberry}

-   [Todo sobre Raspberry]({{< relref "raspberry.md" >}})
-   [Instalación desde cero]({{< relref "raspberry_instalacion.md" >}})
