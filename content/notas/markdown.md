+++
title = "markdown"
author = ["Sherlockes"]
date = 2023-08-30T00:00:00+02:00
lastmod = 2023-09-14T13:19:25+02:00
tags = ["markdown"]
categories = ["script"]
draft = false
weight = 5
thumbnail = "images/markdown.png"
toc = true
+++

Markdown es un lenguaje de marcado ligero creado por John Gruber y Aaron Swartz que trata de conseguir la máxima legibilidad y facilidad de publicación tanto en su forma de entrada como de salida, inspirándose en muchas convenciones existentes para marcar mensajes de correo electrónico usando texto plano.

<!--more-->


## [Hugo CheatSheet](https://sourceforge.net/p/hugo-generator/wiki/markdown_syntax/#md_ex_lists) {#hugo-cheatsheet}


## [Emacs CheatSheet](https://cheatography.com/xaon/cheat-sheets/emacs-markdown-mode/) {#emacs-cheatsheet}
