+++
title = "regexp"
author = ["Sherlockes"]
date = 2023-08-30T00:00:00+02:00
lastmod = 2023-09-21T20:16:13+02:00
tags = ["regexp"]
categories = ["script"]
draft = false
weight = 5
thumbnail = "images/regex.png"
toc = true
+++

En cómputo teórico y teoría de lenguajes formales, una expresión regular, o expresión racional,1​2​ también son conocidas como regex o regexp, por su contracción de las palabras inglesas regular expression, es una secuencia de caracteres que conforma un patrón de búsqueda. Se utilizan principalmente para la búsqueda de patrones de cadenas de caracteres u operaciones de sustituciones.

<!--more-->

-   [CheatSheet (DevHints)](https://devhints.io/regexp)
