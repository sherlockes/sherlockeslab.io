+++
title = "ox-hugo"
author = ["Sherlockes"]
date = 2023-08-30T00:00:00+02:00
lastmod = 2024-01-17T18:02:31+01:00
tags = ["ox-hugo"]
categories = ["emacs"]
draft = false
weight = 5
thumbnail = "images/ox-hugo.png"
toc = true
+++

[Ox-hugo](https://ox-hugo.scripter.co) es una herramienta de [Emacs]({{< relref "emacs.md" >}}) que simplifica la conversión de contenido desde Org Mode a [Markdown]({{< relref "markdown.md" >}}) para su uso con [Hugo]({{< relref "hugo.md" >}}), un generador de sitios web. Facilita la creación de blogs y sitios web, permitiendo a los usuarios escribir en [Org Mode]({{< relref "org_mode.md" >}}) y luego exportar a un formato compatible con Hugo.

<!--more-->


## Documentación Oficial {#documentación-oficial}

-   [Instalación](https://ox-hugo.scripter.co/doc/installation/)


## Fechas {#fechas}

-   Actualizar o insertar fecha actual -&gt; C-u C-u C-c .
-   Fechas - [Enlace](https://ox-hugo.scripter.co/doc/dates/)
-   Actualizar automáticamente la fecha de actualización -&gt; `#+hugo_auto_set_lastmod: t`


## Archivo setup.conf {#archivo-setup-dot-conf}

Para no tener que añadir todos los campos de exportación en dad docuento org, utilizamos un archivo de configuración "setup.conf" tal y como explica en este [artículo](https://github.com/peromage/peromage.github.io/blob/master/blog/setup.conf).


## Exportar {#exportar}


### Problema con la exportación de Links {#problema-con-la-exportación-de-links}

Hay que añadir el siguiente código - [Enlace](https://github.com/kaushalmodi/ox-hugo/issues/483)

```elisp
(setq org-id-extra-files (directory-files-recursively org-roam-directory "\\.org$")))
```

Tienen que estar todos los archivos de Org-Roam en la carpeta raiz, de lo contrario no funciona


### Auto exportar mediante ".dir-locals.el" {#auto-exportar-mediante-dot-dir-locals-dot-el}

Para auto exportar todos los archivos de una carpeta al guardar, segun la documentación [oficial](https://ox-hugo.scripter.co/doc/auto-export-on-saving/#enable-for-the-whole-project) hay que crear un documento ".dir-locals.el" en la raiz del proyecto (Por encima de la carpeta donde están los \*.org) con el siguiente contenido.

```elisp
(("content-org/"
. ((org-mode . ((eval . (org-hugo-auto-export-mode)))))))
```

> El problema de este método es que realiza la función del "hook" al guardar antes que la exportación, por lo que no se actualiza correctamente.


### Auto exportar mediante "hook" al guardar {#auto-exportar-mediante-hook-al-guardar}

Primero tenemos que crear un hook que se ejecute cuando guardamos un archivo dentro de determianda carpeta y posteriormente una función asignada a este hook que exporte el archivo en caso de que sea del tipo "\*.org".

```elisp
(defun funcion-al-guardar ()
  (interactive)
  (if (string= (file-name-extension buffer-file-name) "org")
    (org-hugo-export-wim-to-md :all-subtrees))

  (let ((default-directory "~/brain"))
    (shell-command "gitup")
  )
)

(add-hook 'after-save-hook (lambda () (when (and buffer-file-name (string-prefix-p brain-dir buffer-file-name))(funcion-al-guardar))))
```


## Imágenes {#imágenes}

-   Hay que arrancar correctamente el servidor de hugo `hugo server --navigateToChanged --baseURL=http://localhost:1313/` y el servidor remoto tiene que apuntar directamente a la raiz del proyecto.

-   Incluir una imagen que esté dentro de la carpeta "images" junto con la nota usaremos el la notación `[[./images/pp.png]]`. De esta forma se copiará la imagen al directorio "ox-hugo" dentro de "static" creando una carpeta "ox-hugo".

> Para comprobar a medida que escribimos que la imagen ha sido bien introducida usaremos el ataje de teclado `C-c C-x C-v` (org-display-inline-images) para cambiar al modo de visualización de imagenes insertadas.
