+++
title = "org-mode"
author = ["Sherlockes"]
date = 2023-08-30T00:00:00+02:00
lastmod = 2024-02-08T20:51:24+01:00
tags = ["org-mode"]
categories = ["emacs"]
draft = false
weight = 5
thumbnail = "images/org.png"
toc = true
+++

Un modo principal de GNU [Emacs]({{< relref "emacs.md" >}}) para mantener notas, documentos de autoría, cuadernos computacionales, programación alfabetizada, mantenimiento de listas de tareas, planificación de proyectos y más, en un sistema de texto sin formato rápido y efectivo.

<!--more-->


## Comandos importantes {#comandos-importantes}

-   Insertar un enlace -&gt; C-c C-l
-   Insertar un bloque de código -&gt; C-c C-, s
-   Insertar un trozo de código -&gt; `~codigo~` o `=codigo=`
-   Tachar una palabra -&gt; `Palabra tachada`
-   Insertar un tag -&gt; C-c C-q (C-c C-c)
-   Insertar una cita -&gt; &lt;q TAB
-   Encabezado, Ordenar (org-sort) -&gt; C-c ^
-   Encabezado, Nuevo del mismo nivel -&gt; C-Ret
-   Encabezados, Visualización inicial -&gt; C-u C-u TAB - [Manual](https://orgmode.org/manual/Initial-visibility.html)
-   Línea, Borrar toda la línea (whole-line-or-region.el) -&gt; C-w
-   Línea, Copiar toda la línea (whole-line-or-region.el) -&gt; M-w
-   Lista, Ordenar (org-sort-list) -&gt; C-c ^


## Easy templates (plantillas sencillas) &lt;s Tab [Link](https://www.gnu.org/software/emacs/manual/html_node/org/Easy-templates.html) {#easy-templates--plantillas-sencillas--s-tab-link}


## Modificar las plantillas editando org-structure-template-alist {#modificar-las-plantillas-editando-org-structure-template-alist}


## [Niklasfasching Org mode parser](https://niklasfasching.github.io/go-org/) {#niklasfasching-org-mode-parser}


## Insertar enlace a una función {#insertar-enlace-a-una-función}

Para insertar un enlace a una función de emacs (elisp) en un archivo \*.org se usará el siguiente formato.

```elisp
[[elisp:(función)][Título del enlace]]
```

Tal y como se aclara en este [post](https://emacs.stackexchange.com/questions/71480/how-can-i-link-to-emacs-help-from-org-mode) si queremos que no se no pregunte por la confirmación de la ejecución de la función deberemos editar la variable `~org-link-elisp-skip-confirm-regexp~` e introducir ".\*" como valor.

> También es posible introducirlo directamente en el archivo de configuración de emacs mediante la línea `'(org-link-elisp-skip-confirm-regexp ".*")` dentro del apartado "Custom-set-variables"
