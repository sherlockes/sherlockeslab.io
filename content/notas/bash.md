+++
title = "bash"
author = ["Sherlockes"]
date = 2023-08-30T00:00:00+02:00
lastmod = 2024-04-16T11:15:06+02:00
tags = ["bash"]
categories = ["script"]
draft = false
weight = 5
thumbnail = "images/bash.png"
toc = true
+++

GNU Bash o simplemente Bash (Bourne-again shell) es una interfaz de usuario de línea de comandos popular, específicamente un shell de Unix; así como un lenguaje de scripting. Bash fue originalmente escrito por Brian Fox para el sistema operativo GNU.​ Se ha utilizado ampliamente como el intérprete predeterminado para la mayoría de las distribuciones de GNU/Linux.

<!--more-->


## Alias {#alias}

-   Comprobar si existe el archivo `~/.bash_aliases`
-   Editar el archivo ~/.bashrc si no existe el anterior
-   Añadir el nuevo alias -&gt; alias ll="ls -la"
-   Habilitar los cambios -&gt; source ~/.bashrc


## Comandos {#comandos}


### ls (listar directorio) {#ls--listar-directorio}

-   ls -a (Listar directorio con ocultos)


### mv (mover / renombrar) {#mv--mover-renombrar}

-   mv old-name-dir/ new-name-dir (Renombra directorios)


### sed {#sed}

-   Imprimir una línea -&gt; sed '2p' file


### which (Localizar un comando) {#which--localizar-un-comando}

-   which comando


### Curl {#curl}

-   Descargar un archivo &gt; curl 'dirección' -o archivo


### ps - Kill (Listar y Matar un proceso) {#ps-kill--listar-y-matar-un-proceso}


#### Listar procesos (ps -aux) {#listar-procesos--ps-aux}

-   e = muestra todos los procesos
-   f = formato extra completo
-   a = mostrar procesos para todos los usuarios.
-   u = muestra el usuario propietario del proceso.
-   x = también muestra procesos no conectados a terminal.


#### Matar procesos (kill -9 pid) {#matar-procesos--kill-9-pid}

-   -1 cuelga las llamadas al proceso
-   -9 mata literalmente el proceso
-   -15 termina el proceso


## Grabar imagen en pendrive o sd {#grabar-imagen-en-pendrive-o-sd}

```sh
# localizar la imagen y descomprimirla
unzip loquesea.zip
# localizar la unidad del pendrive
df -h
# desmontar la unidad (o varias...)
umount /dev/sdb1
umount /dev/sdb2
# grabar el pendrive
sudo dd bs=1M if=loquesea.img of=dev/sdb status=progress
```


## Rclone {#rclone}


#### Montar una carpeta &gt; rclone mount ruta_remota: ruta_local --daemon {#montar-una-carpeta-rclone-mount-ruta-remota-ruta-local-daemon}


## Regex {#regex}


#### Uso de variable en una expresión regular -&gt; ${var {#uso-de-variable-en-una-expresión-regular-var}


## Uso de variables indirectas {#uso-de-variables-indirectas}

```sh
var=x; val=foo
eval "$var=\$val"

var=x;x=foo
echo ${!var}
```


## Scripts {#scripts}


### Encabezado {#encabezado}

```nil
#!/bin/bash
# -*- encoding: utf-8 -*-

###################################################################
#Script Name: Nombre
#Description: Descripción
#Args: N/A
#Creation/Update: 20191022/20191129
#Author: www.sherblog.pro
#Email: sherlockes@gmail.com
###################################################################
```


### Lanzar un script y cerrar la terminal `./nombre_del_script.sh &` {#lanzar-un-script-y-cerrar-la-terminal-dot-nombre-del-script-dot-sh-and}

> Si cierras la consola y luego abres una nueva sesión en el mismo terminal, el script se detendrá cuando cierres la sesión de la terminal. Para evitar esto, deberías usar nohup, ya que asegura que el script continúe ejecutándose incluso después de cerrar y abrir una nueva sesión de terminal.


### Parar un script ante un error {#parar-un-script-ante-un-error}

-   set -e
-   set -o errexit

Ambos son equivalentes, con 'set +e' el script no para.


### Directorio de un script {#directorio-de-un-script}

script_dir=\\(( cd -- "\\)( dirname -- "${BASH_SOURCE[0]}" )" &amp;&gt; /dev/null &amp;&amp; pwd )


### Comentarios {#comentarios}

<!--list-separator-->

-  Multilínea -&gt; : '  -------- '
