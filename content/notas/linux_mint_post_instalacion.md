+++
title = "linux mint post instalacion"
author = ["Sherlockes"]
date = 2023-08-30
lastmod = 2023-09-11T11:26:28+02:00
tags = ["linux", "mint"]
categories = ["sos"]
draft = false
weight = 5
thumbnail = "images/mint.png"
toc = true
+++

Con la instalación de [linux mint]({{< relref "linux_mint.md" >}}) no termina la configuración del sistema, todavía realizo todos estos pasos para dejar a mi gusto el resto de aplicaciones.

<!--more-->

El primer paso es la instalación de [Insync](https://www.insynchq.com/) y crear la carpeta "~/GDrive" donde hacer una sincronización selectiva de "dotfiles" y "sherloscripts" para poder seguir adelante con la configuración.

Tras esto hay que:

-   Actualizar el sistema
-   Instalar emacs
-   Instalar hugo
-   Instalar diccionario de Aspell en español
-   Eliminar el compilador de C "gcc" e instalar "clang"
-   Instalar git, generar la llave ssh y copiarla a GitHub
-   Instalación y configuración de Zerotier ([Artículo](https://sherblog.pro/primeros-pasos-con-zerotier/))
-   Instalar [Rclone](https://rclone.org/) y ejecutar la configuración para crear el directorio
-   Ajuste de Dotfiles. [Artículo](https://sherblog.pro/mi-gesti%C3%B3n-de-los-archivos-de-configuraci%C3%B3n/) en Sherblog.pro

Y este es el script completo:

```bash

# Actualizar el sistema
sudo apt update

# Crear el enlace a los Dotfiles
ln -si ~/GDrive/dotfiles ~/dotfiles

# Eliminar el compilador cd c gcc e instalar clang (problemas Sqlite)
sudo apt remove gcc
sudo apt install clang

# Instalación de Emacs y corrección ortográfica
sudo apt install emacs
sudo apt install ispell
sudo apt install aspell-es

# Instalación de Hugo
sudo apt install hugo

# Instalación del cliente para Wireguard
sudo apt install wireguard
sudo apt install resolvc

# Instalación y configuración de git
sudo apt install git
git config --global user.email sherlockes@yahoo.es
git config --global user.name sherlockes

# Instalación de Zerotier y adhesión a red personal
curl -s https://install.zerotier.com/ | sudo bash
sudo zerotier-cli join 83048a0632dad18a

# Instala Rclone y crea el directorio de configuración
sudo -v ; curl https://rclone.org/install.sh | sudo bash
mkdir -p ~/.config/rclone

# Configuración mediante Dotfiles
ln -si ~/dotfiles/bash/.bash_aliases ~/.bash_aliases
ln -si ~/dotfiles/emacs/.emacs ~/.emacs
ln -si ~/dotfiles/emacs/.emacs.d/bookmarks ~/.emacs.d/bookmarks
ln -si ~/dotfiles/ssh/config ~/.ssh/config
ln -si ~/dotfiles/rclone/rclone.conf ~/.config/rclone/rclone.conf

# Eliminar paquetes sobrantes
sudo apt autoremove

# Genera la llave ssh
ssh-keygen
```

-   Tras esto hay que acceder a [Zerotier](https://my.zerotier.com/) para permitir el acceso del ordenador a la red y asignarle un nombre e IP.

-   Tras la instalación del cliente para Wireguard hay que copiar el archivo de configuración tal y como indica este [artículo](https://sherblog.pro/configurar-wireguard-de-home-assistant-en-cliente-linux/).
