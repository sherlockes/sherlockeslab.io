+++
title = "emacs - dired"
author = ["Sherlockes"]
date = 2023-08-31
lastmod = 2023-09-23T12:24:58+02:00
tags = ["emacs", "dired"]
categories = ["apps", "emacs"]
draft = false
weight = 5
thumbnail = "images/emacs.png"
toc = true
+++

Dired crea un buffer en Emacs que contiene una lista de un directorio y, opcionalmente, también algunos de sus subdirectorios. Puede usar los comandos Emacs normales para moverse en este buffer y comandos especiales además para operar en los archivos enumerados. Dired es capz de trabajar con directorios locales y remotos.

<!--more-->


## [Dired Reference Card](https://www.gnu.org/software/emacs/refcards/pdf/dired-ref.pdf) {#dired-reference-card}


## Visor de imágenes (image-dired) {#visor-de-imágenes--image-dired}

-   Miniatura siguiente/anterior -&gt; C-f / C-b
-   Miniatura arriba/abajo -&gt; C-p / C-n
-   Borrar, marcar, desmarcar -&gt; d / m / u
-   Tag, untag -&gt; tt / tu
-   Girar izda/dcha -&gt; l / r
-   Abrir imagen -&gt; Ret
-   Salir -&gt; q
-   Ajustar a ventana -&gt; s
-   Tamaño completo -&gt; f


## Comandos básicos {#comandos-básicos}

-   Buscar archivos en directorio: % m
-   Cambiar el modo de un fichero -&gt; M (Tras seleccionar)
-   Directorio superior -&gt; ^
-   Cambiar opciones ls -&gt; C-u s
-   Ocultar detalles de archivos (dired-hide-details-mode) -&gt; (
-   Directorio, crear nuevo -&gt; +
-   Refrescar -&gt; g
-   Crear enlace simbólico -&gt; S
-   Grep en DIRED (buscar en archivos) -&gt; find-grep-dired
-   Ordenar -&gt; dired-listing-switches [Oremacs](https://oremacs.com/2015/01/13/dired-options/)
-   Renombrar -&gt; C-x C-q (C-c C-c para terminar)


## Mostrar/Ocultar archivos ocultos {#mostrar-ocultar-archivos-ocultos}

Tengo creada la siguiente función asignada a la combinación M-o

```elisp
(define-key dired-mode-map (kbd "M-o")
 (lambda ()
  "Alterna entre mostrar y ocultar"
  (interactive)
  (setq my-dired-switch (- my-dired-switch))
  (if (= my-dired-switch 1)
    (dired-sort-other my-dired-ls-switches-hide)
    (dired-sort-other my-dired-ls-switches-show))))))
```
