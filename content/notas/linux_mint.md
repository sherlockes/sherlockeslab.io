+++
title = "linux mint"
author = ["Sherlockes"]
date = 2023-08-30
lastmod = 2023-09-06T13:14:14+02:00
tags = ["linux", "mint"]
categories = ["sos"]
draft = false
weight = 5
thumbnail = "images/mint.png"
toc = true
+++

Linux Mint es una distribución de GNU/Linux comunitaria de origen franco-irlandesa basada en Ubuntu, y a su vez en Debian, que tiene por objetivo proveer "un sistema operativo moderno, elegante y cómodo que sea tanto poderoso como fácil de usar". Linux Mint soporta varios formatos y códecs multimedia al incluir software propietario y empaquetado con una variedad de aplicaciones gratuitas y de código abierto.

<!--more-->


## [Primeros pasos tras instalacion]({{< relref "linux_mint_post_instalacion.md" >}}) {#primeros-pasos-tras-instalacion--linux-mint-post-instalacion-dot-md}


## Areas de trabajo {#areas-de-trabajo}

-   Cambio entre áreas de trabajo -&gt; Ctrl+Alt+⬆️
-   Siguiente area de trabajo -&gt; Ctrl+Alt+➡️
-   Anterior area de trabajo -&gt; Ctrl+Alt+⬅
-   Mueve la aplicación al siguiente area de trabajo -&gt; Ctrl+Alt+Mayusc+➡️
-   Mueve la aplicación al antgerior area de trabajo -&gt; Ctrl+Alt+Mayusc+⬅️


## Comandos básicos {#comandos-básicos}

-   Alt+espacio -&gt; Abre el menú ventana


## Instalar Emacs 26.3 {#instalar-emacs-26-dot-3}

[Enlace](https://ubunlog.com/llega-la-tercera-version-de-la-rama-26-de-emacs-gnu-emacs-26-3/#Como_instalar_Gnu_Emacs_263_en_Ubuntu_y_derivados)

```bash
sudo add-apt-repository ppa:kelleyk/emacs -y
sudo apt-get update
sudo apt-get install emacs26
```
