+++
title = "home assistant"
author = ["Sherlockes"]
date = 2024-01-17
lastmod = 2024-02-29T16:59:01+01:00
tags = ["ha"]
categories = ["apps", "iot"]
draft = false
weight = 5
thumbnail = "images/homeassistant.png"
toc = true
+++

Home Assistant es una plataforma de domótica de código abierto que transforma hogares en espacios inteligentes. Ofrece integración para una amplia variedad de dispositivos y servicios, permitiendo el control centralizado de luces, termostatos, cámaras y más. Su interfaz intuitiva y automatizaciones personalizables facilitan la gestión del hogar conectado.

<!--more-->


## Notificar batería baja de sensores {#notificar-batería-baja-de-sensores}

-   Importar [este](https://my.home-assistant.io/redirect/blueprint_import/?blueprint_url=https%3A%2F%2Fgist.github.com%2Fsbyx%2F1f6f434f0903b872b84c4302637d0890) blueprint
-   Seguir las [intrucciones](https://community.home-assistant.io/t/low-battery-level-detection-notification-for-all-battery-sensors/258664).


## Añadir Control remoto de Amazon Fire Stick {#añadir-control-remoto-de-amazon-fire-stick}

-   [HA-Firemote](https://github.com/PRProd/HA-Firemote/blob/20fb3dd0ac57261a712dd524c58ded2a132b0446/README.md)

    > Después del arranque o actualización hay que pulsar uno de los botones del mando físico para que empiece a funcionar. Este problema se arregla con un alimentador externo al firestick.


## Casa ZgZ - Galería - Sensor puerta {#casa-zgz-galería-sensor-puerta}

Cuando el sensor de la puerta de la galería pierde la conexión hay que realizar los siguientes pasos:

-   Buscar nuevos dispositivos en la integración ZHA
-   Reiniciar sensor con el clip hasta que parpadea el led tres veces
-   Volver a reiniciar
-   Seleccionar el área de "galería" una vez detectado


## Casa Zgz - Salón - Emparejar GU10 Ikea Tradfri {#casa-zgz-salón-emparejar-gu10-ikea-tradfri}

-   Colocar la bombilla cerca del concentrador Zigbee
-   Encender y apagar 6 veces la bombilla dejándola encendida
-   Buscar nuevos dispositivos en la integración de ZHA

    > En caso de que el dispositivo no esté disponible un tiempo después de haber sido agregado, no es necesario eliminar la instancia, simplemente al buscar nuevos aparecerá ya con el nombre que se la  había asignado anteriormente.


## Zigbee2Mqtt {#zigbee2mqtt}


### Intalar {#intalar}

-   Instalar el addon [Mosquito](https://isytec.net/como-instalar-mosquitto-broker-mqtt-en-home-assistant/)
-   Instalar el addon [Zigbee2Mqtt](https://www.zigbee2mqtt.io/guide/configuration/)
-   Añadir la integración Mqtt
-   Editar "Configuration.yaml" en zigbee2mqtt

<!--listend-->

```yaml
mqtt:
  server: mqtt://core-mosquitto:1883
  user: addons
  password: OuyaiCh5acai7aebohz5Noh3Ziegho0ich7jo8HeicheiH5yahhiaphaiTifog9k
frontend:
  port: 8099
serial:
  port: /dev/ttyUSB1
```
