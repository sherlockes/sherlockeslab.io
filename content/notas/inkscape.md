+++
title = "inkscape"
author = ["Sherlockes"]
date = 2023-08-30T00:00:00+02:00
lastmod = 2023-09-14T19:35:20+02:00
tags = ["inkscape"]
categories = ["apps"]
draft = false
weight = 5
thumbnail = "images/inkscape.png"
toc = true
+++

Inkscape es un editor de gráficos vectoriales libre y de código abierto. Inkscape puede crear y editar diagramas, líneas, gráficos, logotipos, e ilustraciones complejas. El formato principal que utiliza el programa es Scalable Vector Graphics (SVG) versión 1.1.

<!--more-->


## Resetear las preferencias {#resetear-las-preferencias}

Borrar el archivo "~/.config/inkscape/preferences.xml"
