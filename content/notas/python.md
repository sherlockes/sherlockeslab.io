+++
title = "python"
author = ["Sherlockes"]
date = 2023-08-30T00:00:00+02:00
lastmod = 2024-02-11T18:51:32+01:00
tags = ["python"]
categories = ["script"]
draft = false
weight = 5
thumbnail = "images/python.png"
toc = true
+++

Python es un lenguaje de alto nivel de programación interpretado cuya filosofía hace hincapié en la legibilidad de su código, se utiliza para desarrollar aplicaciones de todo tipo, por ejemplo: Instagram, Netflix, Spotify, Panda3D, entre otros.​ Se trata de un lenguaje de programación multiparadigma, ya que soporta parcialmente la orientación a objetos, programación imperativa y, en menor medida, programación funcional.

<!--more-->


## Enlaces pendientes de revisar {#enlaces-pendientes-de-revisar}

Tutorial automate the boring stuff
<https://automatetheboringstuff.com/chapter1/>
Programación orientada a objetos <https://towardsdatascience.com/python-oop-corey-schafer-datacamp-be6b0b3cafc6>
Programación funcional
<https://morioh.com/p/8a40c3345286>


## Python en Emacs {#python-en-emacs}


### Guardar y ejecutar  &gt;   C-c C-c {#guardar-y-ejecutar-c-c-c-c}


### Limpiar el shell    &gt;   C-c M-o {#limpiar-el-shell-c-c-m-o}


## Pandas {#pandas}

import pandas as pd
Cargar csv -&gt; datos = pd.read_csv(ruta/archivo.csv)
Revisar datos -&gt; datos.describe()
Listar campos -&gt; datos.columns
Columna a variable -&gt; y = datos.lacolumnaquesea
Columnas a dataframe -&gt; x = datos[listadecolumnas]
Mostrar primeras filas de dataframe -&gt; x.head()
Eliminar filas con campos vacíos -&gt; filtered_x = x.dropna(axis=0)


## Creación de entornos virtuales {#creación-de-entornos-virtuales}

-   Instalar \`virtualenv\`:

<!--listend-->

```bash
pip install virtualenv
```

-   Crear un nuevo entorno virtual

Ve al directorio en el que deseas crear el entorno virtual y ejecuta el siguiente comando para crear uno nuevo. Puedes reemplazar \`mi_entorno\` con el nombre que prefieras para tu entorno virtual.

```bash
virtualenv mi_entorno
```
