+++
title = "emacs"
author = ["Sherlockes"]
date = 2023-09-04T12:33:00+02:00
lastmod = 2024-03-14T15:20:08+01:00
tags = ["emacs"]
categories = ["apps"]
draft = false
weight = 5
thumbnail = "images/emacs.png"
toc = true
+++

Emacs es un editor de texto reconocido por su amplio conjunto de funciones, lo cual lo ha convertido en una herramienta valorada por programadores y usuarios con orientación técnica. GNU Emacs forma parte del proyecto GNU y destaca como la versión más popular de este editor. El manual de GNU Emacs lo describe como «un editor de texto avanzado, extensible, personalizable y autodocumentado».

<!--more-->


## Grandes temas {#grandes-temas}


### [org-mode]({{< relref "org_mode.md" >}}) {#org-mode--org-mode-dot-md}


### [org-roam]({{< relref "org_roam.md" >}}) {#org-roam--org-roam-dot-md}


### [ox-hugo]({{< relref "ox_hugo.md" >}}) {#ox-hugo--ox-hugo-dot-md}


### [elisp]({{< relref "elisp.md" >}}) {#elisp--elisp-dot-md}


## Pequeños temas {#pequeños-temas}


### Archivos recientes {#archivos-recientes}

Para mostrar en el arranque los últimos archivos editados uso esta combinación.

```elisp
(recentf-mode 1)                   ; Activa el modo recentf
(setq recentf-max-saved-items 50)  ; Ajusta la cantidad de archivos recientes a mostrar.

(add-hook 'after-init-hook (lambda () (recentf-open-files)))
```


### Arranque {#arranque}

-   Arrancar emacs es modo texto desde la terminal: `emacs -nw`


### Ayuda {#ayuda}

-   Prefijo, muestra comandos que empiezan por -&gt; "prefijo" C-h f
-   Atajo, muestra descripción y ayuda (describe-key) -&gt; C-h k
-   Modo, describe el modo activo (describe-mode) -&gt; C-h m
-   Funcion, descripción y ayuda (describe-function) -&gt; C-h f
-   variable, descripción y ayuda (describe-variable) -&gt; C-h v


### Bookmarks (Favoritos) {#bookmarks--favoritos}

-   Añadir --&gt; C-x r m
-   Listar --&gt; C-x r l
    -   ‘a’ – show annotation for the current bookmark
    -   ‘A’ – show all annotations for your bookmarks
    -   ‘d’ – mark various entries for deletion (‘x’ – to delete them)
    -   ‘e’ – edit the annotation for the current bookmark
    -   ‘m’ – mark various entries for display and other operations, (‘v’ – to visit)
    -   ‘o’ – visit the current bookmark in another window, keeping the bookmark list open
    -   ‘C-o’ – switch to the current bookmark in another window
    -   ‘r’ – rename the current bookmark
-   Abrir --&gt; C-x r b
-   Archivo de favoritos - bookmarks-default-file


### Buffers {#buffers}

-   Borrar el buffer completo -&gt; erase-buffer (Sin tecla por defecto)
-   Actualizar/recargar -&gt; revert-buffer-quick
-   Actualizar/recargar archivo (revert-buffer) -&gt; C-x C-v Ret
-   Actualizar/recargar automáticamente un buffer abierto -&gt; auto-revert-mode
-   Recargar archivo (añade al final) (auto-revert-tail-mode)
-   Siguiente|anterior buffer (previous-buffer|next-buffer) -&gt; C-x &lt;left&gt; | C-x &lt;right&gt;
-   Cuardar como (buffer) -&gt; C-x C-w [Link](https://www.gnu.org/software/emacs/manual/html_node/emacs/Save-Commands.html)
-   Guardar como (todo el texto) -&gt; C-x h M-x write-region
-   Instalar paquete desde buffer -&gt; install-package-from-buffer
-   Mostrar los números de línea -&gt; linum-mode
-   Seleccionar el buffer completo -&gt; C-x h


### Busqueda {#busqueda}

-   Buscar (Isearch) -&gt; C-S
-   Repetir búsqueda -&gt; C-s C-s
-   Añadir siguiente palabra -&gt; C-w
-   Volver al inicio -&gt; C-u C-&lt;spc&gt;


### Corrección Ortográfica y Diccionario {#corrección-ortográfica-y-diccionario}

-   Cambiar diccionario -&gt; M-x ispell-change dictionary "castellano"
-   Instalar diccionario -&gt; [Onda Hostil](https://ondahostil.wordpress.com/2017/01/17/lo-que-he-aprendido-configurando-aspell-para-emacs/)
    -   Descargarlo de [GNU-Aspell](ftp://ftp.gnu.org/gnu/aspell/dict/)
    -   Descomprimirlo
    -   Ejecutar lo siguiente desde la carpeta descomprimida
        ```bash
        ./configure
        make
        sudo make install
        make clean
        ```
-   Corregir el buffer -&gt; M-x ispell-minor-mode
-   Subrayado -&gt; M-x flyspell-mode


### Edición {#edición}

-   Cadena, reemplazar (replace-string) -&gt; M-%
-   Caracter, corta hasta (zap-to-char) -&gt; M-z
-   Carácteres, transponer (transpose-chars) -&gt; C-t
-   Deshacer (Undo) -&gt; C-/ | C-x u | C-\_
-   Expresiones, transponer (transpose-sexps) -&gt; C-M-t
-   Expresión regular, reemplazar (replace-regexp) -&gt; C-M-%
-   Imagen, editar archivos en modo texto &gt; C-c C-c (auto-image-file-mode 1)
-   Línea, borrar -&gt; C-S-Backspace
-   Línea, comentar (comment-dwim) -&gt; M-;
-   Línea, comentar (comment-line) -&gt; C-x C-;
-   Línea, copiar (Instalar/habilitar paquete whole-line-or-region) -&gt; M-w
-   Línea, corta completa (Instalar/habilitar paquete whole-line-or-region) -&gt; C-w
-   Línea, corta hasta el final (kill-line) -&gt; C-k
-   Línea, corta línea completa (kill-whole-line) -&gt; C-S-&lt;backspace&gt;
-   Línea, insertaren blanco a continuación, insertar (open-line) -&gt; C-o
-   Líneas, borrar en blanco (delete-blank-lines) -&gt; C-x C-o
-   Líneas, ordenar selección (sort-lines) - Artículo en [Susam.net](https://susam.net/blog/sorting-in-emacs.html)
-   Líneas, sangrar varias líneas 8 -&gt; C-u 8 C-x Tab
-   Líneas, transponer líneas (transpose-lines) -&gt; C-x C-t
-   Oraciones, transponer (transpose-sentences)
-   Palabra anterior, corta (backward-kill-word) -&gt; M-&lt;back&gt;
-   Palabra siguiente a mayúsculas (upcase-word) -&gt; M-u
-   Palabra siguiente a minúsculas (downcase-word) -&gt; M-l
-   Palabra siguiente, corta (kill-word) -&gt; M-d
-   Palabra, capitalizar (capitalize-word) -&gt; M-c
-   Párrafos, transponer (transpose-paragraphs)
-   Reemplazar ^M por un salto de línea &gt; M-x replace-string C-q C-m RET RET
-   Seleccionar todo -&gt; C-x h
-   Selección , corta (kill-region) -&gt; C-w
-   Selección a mayúsculas (upcase-region) -&gt; C-x C-u
-   Selección a minúsculas selección (downcase-region) -&gt; C-x C-u
-   Selección, capitalizar (upcase-initials-region)
-   Selección, copiar al anillo (kill-ring-save) -&gt; M-w
-   Selección, copiar selección, añadir (append-next-kill) -&gt; C-M-w


### Evaluando código (org-babel) {#evaluando-código--org-babel}

-   Comando python no encontrado: Añadir a init.el `(setq org-babel-python-command "python3")`
-   Obtener resultados para "Print"-&gt; Declarar `#+BEGIN_SRC python :results output`
-   Reemplazar Jupiter Notebook por Emacs Org Mode -&gt; [Michael Neuper](https://michaelneuper.com/posts/replace-jupyter-notebook-with-emacs-org-mode/)
-   Seleccionar los lenguajes a evaluar -&gt; Editar la variable `org-babel-load-languages`


### Macros {#macros}

-   Iniciar grabación de macro (start-kbd-macro) -&gt; &lt;f3&gt;  ò  C-x (
-   Parar grabación de macro (stop-kbd-macro) -&gt; &lt;f4&gt;  ò C-x )


### MarkDown Mode {#markdown-mode}

-   Insertar encabezado -&gt; C-c C-s


### Movimiento del cursor {#movimiento-del-cursor}

-   Buffer, inicio/Fin de buffer --&gt; M-&lt;/M-&gt;
-   Buffer, retornar a posición anterior --&gt; C-u C-&lt;spc&gt;
-   Encabezado anterior mismo nivel (backward-same-level) -&gt; C-c C-b
-   Encabezado anterior (outline-previous-visible-heading) -&gt; C-c C-p
-   Encabezado inmediatamente superior (outline-up-heading) -&gt; C-c C-u
-   Encabezado siguiente mismo nivel (outline-forward-same-level) -&gt; C-c C-f
-   Encabezado siguiente (outline-up-heading) -&gt; C-c C-n
-   Línea, ir a línea determinada: M-g g (goto-line)
-   Línea, mover a la anterior: C-p
-   Línea, mover a la siguiente: C-n
-   Palabra, mueve a la anterior: M-b
-   Palabra, mueve a la siguiente: M-f
-   Mueve al primer espacio no blanco -&gt; M-n
-   Scroll Abajo/Arriba --&gt; C-v / M-v
-   Scroll Abajo/Arriba (Otra ventana) --&gt; C-M-v / C-M-S-v (C-M-- C-M-v)
-   Scroll Horizontal --&gt; C-x &lt; / C-x &gt;
-   Volver a la posición anterior --&gt; C-u C-spc


### Navegación {#navegación}

-   Abrir un directorio remoto ssh C-x d /ssh:user@host:folder
-   Abrir una url en buffer -&gt; browse-url-emacs


#### Ocur Mode (Buscar por líneas) M-x occur / M-s o {#ocur-mode--buscar-por-líneas--m-x-occur-m-s-o}

-   Sig/anterior coincidencia en bufer occur --&gt; M-n M-p
-   Refrescar buffer --&gt; g
-   Salir --&gt; q
-   Editar en buffer ocur --&gt; e
-   Guardar la edición --&gt; C-c C-c
-   En varios buffers --&gt; M-x multi-occur
-   En varios archivos (\*.py) --&gt; M-x multi-occur \\


### Paquetes {#paquetes}

-   Instalar paquete desde archivo -&gt; package-install-file
-   Yasnippet
    -   Nuevo Snippet -&gt; yas-new-snippet
    -   Editar Snippet -&gt; yas-visit-snippet-file
    -   Ver snippets de un tipo de archivo -&gt; Alt+x yas-describe-tables
    -   Configurar directorio de Snippets -&gt; (setq yas-snippet-dirs (list(concat user-dir "_dotfiles/emacs_.emacs.d/snippets")))
        Sin el "list" no evalúa correctamente y no funciona.
-   [Dired]({{< relref "emacs_dired.md" >}})


### Repetir último comando ([Info](http://xahlee.info/emacs/emacs/emacs_repeat_command.html)] -&gt; C-x z {#repetir-último-comando--info-c-x-z}


### Recuperar archivos -&gt; m-x recover-file {#recuperar-archivos-m-x-recover-file}


### Selección {#selección}

-   Activar la marca de posición -&gt; C-spc
-   Marcar el siguiente párrafo -&gt; M-h
-   Marcar todo el buffer -&gt; C-x h
-   Marcar una función -&gt; C-M-h
-   Marcar la siguiente palabra -&gt; M-@
-   Marcar las dos siguiente palabras - M-2 M-@
-   Marcar la siguiente expresión -&gt; C-M-@
-   Marcar desde el último punto de salto --&gt; C-x C-x
-   Desactivar la marca -&gt; C-u C-&lt;spc&gt;


### Shell {#shell}

-   Abrir la consola de comandos -&gt; M-x shell
-   Ejecutar un archivo (executable-interpret) -&gt; C-c C-x
-   Abrir la terminal -&gt; M-x shell
-   Terminar la ejecución -&gt; C-c C-c
-   Ejecutar comando anterior -&gt; M-p
-   Permitir la ejecución de alias ([Info](https://emacs.stackexchange.com/questions/56358/emacs-shell-not-picking-aliases-from-bashrc))
    ```elisp
    (setq shell-command-switch "-ic")
    ```


### Temas {#temas}

-   Cambiar el tema -&gt; M-x customize-themes


### Ventanas {#ventanas}

-   Eliminar la ventana actual -&gt; C-x 0
-   Eliminar el resto de ventanas -&gt; C-x 1
-   División horizontal -&gt; C-x 2
-   División vertical -&gt; C-x 3
-   Cambio de ventanas con Ace-window
    -   Instalar "ace-window" desde el repositorio de Melpa.
    -   Añadir la línea "(global-set-key (kbd "M-o") 'ace-window)" al archivo de configuración.
    -   Cambiar de ventana mediante M-o y el número que se deseb


### Visualización {#visualización}

-   Ajuste de línea(cambiar) -&gt; toggle-truncate-lines
