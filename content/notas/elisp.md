+++
title = "elisp"
author = ["Sherlockes"]
date = 2023-09-04T12:33:00+02:00
lastmod = 2023-09-14T19:35:19+02:00
tags = ["elisp", [",", "emacs"]]
categories = ["script"]
draft = false
weight = 5
thumbnail = "images/elisp.png"
toc = true
+++

Emacs Lisp es un dialecto del lenguaje de programación Lisp que se usa en los editores GNU Emacs y XEmacs. Emacs Lisp es a veces llamado Elisp, lo cual tiene el riesgo de llevar a confundirlo con otro dialecto de Lisp que tiene el mismo nombre.

La mayoría de las funciones de edición de Emacs vienen de código escrito en Lisp; el resto está escrito en C. Los usuarios que deseen personalizar o extender las funcionalidades de Emacs pueden escribir código en Emacs Lisp.

<!--more-->


## Variables {#variables}

-   Declarar Variable global -&gt; (setq variable valor)


## Archivo {#archivo}

-   Escribir a un archivo -&gt; (write-region "loquesea" nil "~/archivo")
-   Añadir a un archivo -&gt; (append-to-file "loquesea" nil "~/archivo")


## Shell-command {#shell-command}

-   Permitir el uso de alias ([Info](https://stackoverflow.com/questions/12224909/is-there-a-way-to-get-my-emacs-to-recognize-my-bash-aliases-and-custom-functions))

Hay que añadir las siguientes variables en el archivo de configuración principal

```elisp
(setq explicit-shell-file-name "/bin/bash")
(setq shell-file-name "bash")
(setq explicit-bash.exe-args '("--noediting" "--login" "-ic"))
(setq shell-command-switch "-ic")
(setenv "SHELL" shell-file-name)
```
