+++
title = "strava"
author = ["Sherlockes"]
date = 2023-10-31
lastmod = 2024-04-10T11:34:19+02:00
tags = ["python"]
categories = ["script"]
draft = false
weight = 5
thumbnail = "images/image.jpg"
toc = true
+++

Lo que pretende una creación de un propio "heatmap" a partir de mis actividades de Strava usando [python]({{< relref "python.md" >}}). Veremos hasta donde llegamos y por donde pasamos...

<!--more-->

Tenemos creado un entorno virtual en la carpeta "/home/sherlockes/Temp/strava" según está descrito en el nodo de [python]({{< relref "python.md" >}}).


## Bibliotecas y módulos necesarios {#bibliotecas-y-módulos-necesarios}

-   [Stravalib](https://stravalib.readthedocs.io/en/latest/) proporciona una interfaz fácil de usar para interactuar con la API de Strava, una plataforma popular para el seguimiento y análisis de actividades deportivas. Con Stravalib, los desarrolladores pueden acceder a datos de actividades, segmentos, atletas y más para crear aplicaciones relacionadas con el fitness.
-   [python-dotenv](https://pypi.org/project/python-dotenv/) facilita la gestión de variables de entorno en proyectos. Permite cargar variables desde archivos .env en el entorno de desarrollo, lo que simplifica la configuración y la portabilidad del código. Es útil para almacenar credenciales y configuraciones sensibles de manera segura y modular.
-   [Flask](https://flask.palletsprojects.com) es un framework web minimalista que facilita la creación de aplicaciones web rápidas y escalables. Con una sintaxis simple y flexible, Flask ofrece extensiones para funcionalidades adicionales, como autenticación, bases de datos y API RESTful. Es popular por su facilidad de uso y su comunidad activa.
-   [requests](https://pypi.org/project/requests/) es utilizada para realizar solicitudes HTTP de manera sencilla y elegante. Ofrece una interfaz amigable para enviar solicitudes GET, POST, PUT, DELETE y más. Con Requests, los desarrolladores pueden interactuar con APIs web y realizar tareas como la descarga de contenido o el envío de datos fácilmente.
-   [webbrowser](https://docs.python.org/3/library/webbrowser.html) es un módulo de Python que proporciona una interfaz para abrir y controlar navegadores web desde programas Python. Permite abrir una URL en el navegador predeterminado del sistema, así como también proporciona funciones para abrir nuevas pestañas, nuevas ventanas y controlar la navegación web de manera programática. Es útil para automatizar tareas de navegación web y lanzar aplicaciones web desde scripts.
-   [datetime](https://docs.python.org/3/library/datetime.html) es un módulo en Python que proporciona clases para manejar fechas y horas de manera eficiente. Permite crear objetos de fecha y hora, realizar cálculos y operaciones con ellos, formatear y analizar fechas y horas, y trabajar con zonas horarias. Es fundamental para trabajar con datos temporales en aplicaciones Python, como la gestión de eventos, programación, análisis de series temporales y más.
-   [os](https://docs.python.org/3/library/os.html) proporciona funciones para interactuar con el sistema operativo subyacente. Permite realizar operaciones como navegación de directorios, manipulación de archivos, gestión de procesos, acceso a variables de entorno y más. Es esencial para tareas relacionadas con el sistema de archivos y la administración de recursos en programas Python.
-   [logging](https://docs.python.org/3/library/logging.html) facilita la implementación de registro de eventos en aplicaciones. Permite registrar mensajes de diferentes niveles de gravedad, como DEBUG, INFO, WARNING, ERROR y CRITICAL, en varios destinos, como la consola, archivos, o incluso a través de red. Esto ayuda en la depuración, monitoreo y seguimiento de errores en el código de manera estructurada y configurable.


## Credenciales {#credenciales}

Necesitamos las credenciales "client_id" y "client_secret" para el acceso a la API de Strava que almacenaremos en la carpeta donde vamos a guardar los datos. Meteremos estas credenciales en un archivo ".env" con la siguiente forma:

```txt
CLIENT_ID=*******
CLIENT_SECRET=*************************
```

Para cargar estas credenciales como variables a la hora de usar el script emplearemos la librería [python-dotenv](https://pypi.org/project/python-dotenv/) con las siguientes líneas:

```python
ruta_strava = '/home/sherlockes/strava/'
ruta_archivo_env = ruta_strava + '.env'
load_dotenv(ruta_archivo_env)
client_id = os.getenv('CLIENT_ID')
client_secret = os.getenv('CLIENT_SECRET')
```


## Script en python {#script-en-python}

```python
from stravalib.client import Client

client = Client()
authorize_url = client.authorization_url(
    client_id=114649, redirect_uri="http://localhost:8282/authorized"
)
```
