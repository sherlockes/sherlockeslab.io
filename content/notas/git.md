+++
title = "git"
author = ["Sherlockes"]
date = 2023-08-30T00:00:00+02:00
lastmod = 2024-04-18T17:50:30+02:00
tags = ["git"]
categories = ["apps"]
draft = false
weight = 5
thumbnail = "images/git.png"
toc = true
+++

Git es un software de control de versiones diseñado por Linus Torvalds, pensando en la eficiencia, la confiabilidad y compatibilidad del mantenimiento de versiones de aplicaciones cuando estas tienen un gran número de archivos de código fuente. Su propósito es llevar registro de los cambios en archivos de computadora incluyendo coordinar el trabajo que varias personas realizan sobre archivos compartidos en un repositorio de código.

<!--more-->


### Borrar último commit (Si el repositorio local y el remoto están sincronizados) {#borrar-último-commit--si-el-repositorio-local-y-el-remoto-están-sincronizados}

git reset HEAD^ --hard
git push origin -f


### Actualizar el repositorio local -&gt; git fetch {#actualizar-el-repositorio-local-git-fetch}


## Gitlab {#gitlab}


### Servir directorio como html estático {#servir-directorio-como-html-estático}

Este es el contenido del archivo ".gitlab-ci.yml" situado en la raiz del repositorio, se publicará la carpeta "public"

```yaml
  image: busybox

pages:
  stage: deploy
  environment: production
  script:
    - echo "The site will be deployed to $CI_PAGES_URL"
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```
